package demo.services.rentit;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;

import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import demo.Application;
import demo.integration.dto.PlantResource;
import demo.integration.dto.PurchaseOrderResource;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class RentalServiceTests {
	@Autowired
	RestTemplate restTemplate;
	
	@Autowired
	RentalService rentitProxy;
	
	List<ClientHttpRequestInterceptor> interceptors;
	
	@Before
	public void setUp() {
		interceptors = restTemplate.getInterceptors();
	}
	@After
	public void tearDown() {
		restTemplate.setInterceptors(interceptors);
	}
	
	@Test(expected=PlantNotAvailableException.class)
	public void testRejection() throws RestClientException, PlantNotAvailableException {
		ClientHttpRequestInterceptor interceptor = new ClientHttpRequestInterceptor() {			
			public ClientHttpResponse intercept(HttpRequest request, byte[] body,
					ClientHttpRequestExecution execution) throws IOException {
				request.getHeaders().add("prefer", "409");
				return execution.execute(request, body);
			}
		};
		restTemplate.setInterceptors(Collections.singletonList(interceptor));
		PlantResource plant = new PlantResource();
		plant.add(new Link("http://localhost:9000/rest/plants/10001"));
		Date startDate = new Date();
		Date endDate = new Date();
		
		rentitProxy.createPurchaseOrder(plant, startDate, endDate);		
	}

	@Test
	public void testConfirmation() throws RestClientException, PlantNotAvailableException {
		PlantResource plant = new PlantResource();
		plant.add(new Link("http://localhost:9000/rest/plants/10001"));
		Date startDate = new Date();
		Date endDate = new Date();
		
		PurchaseOrderResource result = rentitProxy.createPurchaseOrder(plant, startDate, endDate);
		
		Assert.assertThat(result, is(notNullValue()));
	}
}
