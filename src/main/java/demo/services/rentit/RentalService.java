package demo.services.rentit;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import demo.integration.dto.PlantResource;
import demo.integration.dto.PurchaseOrderResource;

@Service
public class RentalService {
	@Autowired
	RestTemplate restTemplate;
		
	public PurchaseOrderResource createPurchaseOrder(PlantResource plant, Date startDate, Date endDate) throws RestClientException, PlantNotAvailableException {
		PurchaseOrderResource po = new PurchaseOrderResource();
		po.setPlant(plant);
		po.setStartDate(startDate);
		po.setEndDate(endDate);
		
		ResponseEntity<PurchaseOrderResource> result = restTemplate.postForEntity("http://localhost:9000/rest/pos", po, PurchaseOrderResource.class);
		
		if (result.getStatusCode().equals(HttpStatus.CONFLICT))
			throw new PlantNotAvailableException();
		
		return result.getBody();
	}
}
