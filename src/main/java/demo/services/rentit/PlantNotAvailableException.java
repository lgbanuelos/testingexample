package demo.services.rentit;

public class PlantNotAvailableException extends Exception {
	private static final long serialVersionUID = 1L;

	public PlantNotAvailableException() {
		super();
	}
}
